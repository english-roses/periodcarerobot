package furhatos.app.periodcarer.flow

import furhatos.app.periodcarer.nlu.ContraceptiveMethodsList
import furhatos.app.periodcarer.nlu.MenstruationProductsList
import furhatos.app.periodcarer.nlu.MenstruationTopicList
import furhatos.records.User

class MenstruationTopicData (
        var menstruationTopics: MenstruationTopicList = MenstruationTopicList()
)

val User.selectedMenstruationTopic : MenstruationTopicData
    get() = data.getOrPut(Menstruation::class.qualifiedName, MenstruationTopicData())


class ContraceptiveMethodData (
        var contraceptiveMethods: ContraceptiveMethodsList = ContraceptiveMethodsList()
)

val User.selectedContraceptiveMethods : ContraceptiveMethodData
    get() = data.getOrPut(ContraceptionMethods::class.qualifiedName, ContraceptiveMethodData())

class MenstruationProductData (
        var menstruationProducts: MenstruationProductsList = MenstruationProductsList()
)

val User.selectedMenstruationProducts : MenstruationProductData
    get() = data.getOrPut(MenstruationProducts::class.qualifiedName, MenstruationProductData())
