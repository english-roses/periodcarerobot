package furhatos.app.periodcarer.nlu

import furhatos.nlu.*
import furhatos.util.Language

class GoodbyeKeywords: Intent() {
    override fun getExamples(lang: Language) : List<String> {
        return listOf(
                "Nothing", "None", "I want to stop", "This conversation makes me uncomfortable", "Stop"
        )
    }
}

class MenstruationKeywords: Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf(
                "Menstruation"
        )
    }
}

class ContraceptionKeywords: Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf(
                "Contraception"
        )
    }
}

class MenstrProdKeywords: Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf(
                "Menstruation Products"
        )
    }
}

class MenstruationTopic: EnumEntity(stemming = true, speechRecPhrases = true) {
    override fun getEnum(lang: Language): List<String> {
        return listOf("why do I have menstruation", "what is menstruation", "what is the impact of menstruation")
    }
}

class ChooseMenstruationTopic(var menstruationTopics: MenstruationTopicList? = null) : Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf("@menstruationTopics", "I want to learn about @menstruationTopics")
    }
}

class RequestforMenstruationTopic: Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf("What options do you have?",
                "Which are the contraceptive methods?",
                "What are the alternatives?"
        )
    }
}

class MenstruationTopicList : ListEntity<QuantifiedMenstruationTopic>()

class QuantifiedMenstruationTopic(
        val menstruationTopic: MenstruationTopic? = null) : ComplexEnumEntity() {

    override fun getEnum(lang: Language): List<String> {
        return listOf("@menstruationTopic")
    }

    override fun toText(): String {
        return generate("$menstruationTopic")
    }
}

//Contraceptive Methods entity
class ContraceptiveMethod: EnumEntity(stemming = true, speechRecPhrases = true) {
    override fun getEnum(lang: Language): List<String> {
        return listOf("female condom", "emergency contraceptives", "birth control pills", "vaginal ring",
                "injected contraceptives", "intrauterine devices", "contraceptive diaphragm or cap")
    }
}

// Our Choose a Contraceptive method intent
class ChooseContraceptiveMethod(var contraceptiveMethods: ContraceptiveMethodsList? = null) : Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf("@contraceptiveMethods", "I want to learn about @contraceptiveMethods")
    }
}

class RequestContraceptiveMethods: Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf("What options do you have?",
                "Which are the contraceptive methods?",
                "What are the alternatives?"
        )
    }
}

class ContraceptiveMethodsList : ListEntity<QuantifiedContraceptiveMethod>()

class QuantifiedContraceptiveMethod(
        val contraceptiveMethod: ContraceptiveMethod? = null) : ComplexEnumEntity() {

    override fun getEnum(lang: Language): List<String> {
        return listOf("@contraceptiveMethod")
    }

    override fun toText(): String {
        return generate("$contraceptiveMethod")
    }
}

//Menstruation Products entity
class MenstruationProductsEntity: EnumEntity(stemming = true, speechRecPhrases = true) {
    override fun getEnum(lang: Language): List<String> {
        return listOf("tampons", "menstrual cup", "pads")
    }
}

// Menstruation Products intent
class MenstruationProductsIntent(var menstruationProducts: MenstruationProductsList? = null) : Intent() {
    override fun getExamples(lang: Language): List<String> {
        return listOf("@menstruationProducts", "I want to learn about @menstruationProducts")
    }
}

class MenstruationProductsList : ListEntity<QuantifiedMenstruationProduct>()

class QuantifiedMenstruationProduct(
        val menstruationProduct: MenstruationProductsEntity? = null) : ComplexEnumEntity() {

    override fun getEnum(lang: Language): List<String> {
        return listOf("@menstruationProduct")
    }

    override fun toText(): String {
        return generate("$menstruationProduct")
    }
}