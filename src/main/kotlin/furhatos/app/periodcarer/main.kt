package furhatos.app.periodcarer

import furhatos.app.periodcarer.flow.*
import furhatos.skills.Skill
import furhatos.flow.kotlin.*

class PeriodcarerSkill : Skill() {
    override fun start() {
        Flow().run(Idle)
    }
}

fun main(args: Array<String>) {
    Skill.main(args)
}
