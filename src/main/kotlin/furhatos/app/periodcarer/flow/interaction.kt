package furhatos.app.periodcarer.flow

import furhatos.app.periodcarer.nlu.*
import furhatos.flow.kotlin.State
import furhatos.flow.kotlin.furhat
import furhatos.flow.kotlin.onResponse
import furhatos.flow.kotlin.state
import furhatos.gestures.Gestures
import furhatos.nlu.common.No
import furhatos.nlu.common.Yes

val Start : State = state(Interaction) {
    onEntry {
        furhat.say {
            + "Hello, my name is Rose. I am a menstruation-awareness robot."
            + Gestures.Wink
        }
        goto(StartConversation)
    }
}

val StartConversation = state {

    onEntry {
        furhat.say ( "I am here to help you with information about menstruation and contraception. Maybe this " +
                "conversation will make you feel uncomfortable. If so, please tell me." )
        furhat.gesture(Gestures.Smile)
        furhat.ask("Okay?")
    }

    onResponse<No> {
        furhat.gesture(Gestures.ExpressSad)
        furhat.say("Okay, that's a shame. Have a splendid day!")
        goto(Idle)
    }

    onResponse<Yes> {
        furhat.gesture(Gestures.Nod)
        furhat.say ( "Great, let's start!" )
        goto(ChooseTopic)
    }
}

val ChooseTopic : State = state(Interaction) {
    onEntry {
        furhat.ask("Which of the following topics make you feel least comfortable talking about? Menstruation, " +
                "contraception or menstruation products? In case you want to stop you can say 'I want to stop' after "+
                "any question.")
    }

    onResponse<MenstruationKeywords> {
        goto(ChooseMenstruation)
    }

    onResponse<ContraceptionKeywords> {
        goto(ChooseContraceptiveMethod)
    }

    onResponse<MenstrProdKeywords> {
        goto(ChooseMenstruationProducts)
    }

    onResponse<GoodbyeKeywords> {
        furhat.say(
                "Ok bye!"
        )
        goto(Idle)
    }
}

var Menstruation = state(Interaction) {
    onResponse<ChooseMenstruationTopic> {
        val menstruationTopics = it.intent.menstruationTopics
        if (menstruationTopics != null) {
            goto(MenstruationTopicSelected(menstruationTopics))
        }
        else {
            propagate()
        }
    }

    onResponse<RequestforMenstruationTopic> {
        furhat.gesture(Gestures.Nod)
        furhat.ask("There are so many things to tell you about menstruation. Currently I can answer you why do you " +
                "have menstruation, what is menstruation and what is the impact of menstruation on you. Which part do you want to understand?")
    }

    onResponse<Yes> {
        furhat.ask("Currently I can answer you why do you have menstruation, what is menstruation and what is the " +
                "impact of menstruation on you. Which part do you want to understand?")
    }

    onResponse<GoodbyeKeywords> {
        furhat.say(
                "Okay, that's a shame. Have a splendid day."
        )
        goto(Idle)
    }
}

fun MenstruationTopicSelected(menstruationTopicList: MenstruationTopicList) : State = state (Menstruation){
    onEntry {
        furhat.say(
                "${menstruationTopicList.text}!"
        )

        when (menstruationTopicList.text) {
            "why do I have menstruation" -> {
                furhat.gesture(Gestures.Smile)
                furhat.say("That is  an amazing process! As a woman, your period is your body’s way of releasing " +
                        "tissue that it no longer needs. Every month, your body prepares for pregnancy. The lining of " +
                        "your uterus gets thicker as preparation for nurturing a fertilized egg. An egg is released " +
                        "and is ready to be fertilized and settled in the lining of your uterus. If the egg is not " +
                        "fertilized, your body no longer needs the thicker lining of the uterus, so it starts to " +
                        "break down and is eventually expelled, along with some blood, from your vagina. This is " +
                        "your period, and once it’s over, the process starts all over again.")
            }
            "what is menstruation" -> {
                furhat.gesture(Gestures.Wink)
                furhat.say("There are four main phases of the menstrual cycle: menstruation, the follicular phase, " +
                        "ovulation, the luteal phase. Menstruation, or period, is normal vaginal bleeding that occurs as " +
                        "part of a woman's monthly cycle. Every month, your body prepares for pregnancy. If no pregnancy " +
                        "occurs, the uterus, or womb, sheds its lining. The menstrual blood is partly blood and partly " +
                        "tissue from inside the uterus. It passes out of the body through the vagina.")
            }
            "what is the impact of menstruation" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("Before and during menstruation, your body will send you some signals. Besides the " +
                        "bleeding, other signs and symptoms of menstruation may include headache, acne, bloating, pains " +
                        "in the low abdomen, tiredness, mood changes, food cravings, breast soreness, and diarrhoea. " +
                        "Stress from extreme or traumatic events has been linked to dramatic changes in normal " +
                        "menstruation. War, separation from family, and famine have been anecdotally linked to " +
                        "amenorrhea in physician and epidemiological reports. Physical, emotional, and sexual abuse " +
                        "have been associated with the development of premenstrual syndrome (PMS) and premenstrual " +
                        "dysphoric disorder (PMDD). Post-traumatic stress disorder (PTSD) has also been associated " +
                        "with PMDD. Therefore, when you are emotionally unstable during your menstrual period, " +
                        "please do not blame yourself. This is a natural phenomenon.")
            }
        }

        furhat.ask("You have heard about ${menstruationTopicList.text}. Do you want to continue in the same category?")

    }

    onReentry {
        furhat.ask("Do you want anything else?")
    }

    onResponse<No> {
        furhat.say("Okay, no problem.")
        goto(ChooseTopic)
    }

    onResponse<Yes> {
        furhat.say("Okay cool.")
        goto(ChooseMenstruation)
    }

}

var ChooseMenstruation = state(parent = Menstruation) {

    onEntry {
        furhat.gesture(Gestures.Thoughtful)
        furhat.say("Hm..., that's a very interesting topic. Menstruation is complicated and affects women in so " +
                "many ways. I haven't truly experience this but I do really admire you for tolerate this every month!")
        Gestures.Wink(strength = 10.0, duration = 2.0)
        furhat.ask("There are so many things to tell you about menstruation. Currently I can answer you why do you " +
                "have menstruation, what is menstruation and which is  the impact of menstruation on you. Which part do you want to understand?")
    }

    onResponse<No> {
        furhat.say("Okay. Let's see if you have other interested topic")
        goto(ChooseTopic)
    }
}

var ContraceptionMethods = state (Interaction) {
    onResponse<ChooseContraceptiveMethod> {
        val contraceptiveMethods = it.intent.contraceptiveMethods
        if (contraceptiveMethods != null) {
            goto(ContraceptiveMethodSelected(contraceptiveMethods))
        }
        else {
            propagate()
        }
    }

    onResponse<RequestContraceptiveMethods> {
        furhat.gesture(Gestures.BrowFrown)
        furhat.ask("I am familiar with the following methods: female condom, contraceptive diaphragm or cap, birth " +
                "control pills, injected contraceptives, vaginal ring or intrauterine devices. Choose one to discuss more.")
    }

    onResponse<Yes> {
        furhat.gesture(Gestures.Oh)
        furhat.ask("I am familiar with the following methods: female condom, contraceptive diaphragm or cap, birth " +
                "control pills, injected contraceptives, vaginal ring or intrauterine devices. Choose one to discuss more.")
    }

    onResponse<GoodbyeKeywords> {
        furhat.say(
                "Ok bye!"
        )
        goto(Idle)
    }
}

fun ContraceptiveMethodSelected(contraceptiveMethodsList: ContraceptiveMethodsList) : State = state (ContraceptionMethods){
    onEntry {
        furhat.say(
                "${contraceptiveMethodsList.text}!"
        )

        when (contraceptiveMethodsList.text) {
            "vaginal ring" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("This small, flexible ring is inserted high in the vagina, and releases estrogen and " +
                        "progestin, which prevent ovulation. The vaginal ring is usually left in for three weeks, " +
                        "and then removed for a week to allow menstruation to occur but it can be used continuously " +
                        "or in an extended fashion with a new ring every month. Spotting between periods may happen, " +
                        "particularly in the first three months.")
            }
            "birth control pills" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("Birth control pills were originally only packaged as 28 pills – 21 pills containing the" +
                        " hormone(s) required to suppress ovulation, and 7 placebo pills. The 7 days of placebo were " +
                        "designed to allow menstruation to occur. No matter which birth control pill you are taking, " +
                        "you may experience irregular spotting or bleeding during the first few months of taking the " +
                        "birth control pill. Spotting can also result from forgetting to take a pill, or taking it late." +
                        " Taking your pill even a few hours later than normal can cause spotting, especially with the progestin-only pill.")
            }
            "injected contraceptives" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("The contraceptive injection (Depo-Provera, Sayana Press or Noristerat) releases the " +
                        "hormone progestogen into your bloodstream to prevent pregnancy. Depo-Provera is most commonly " +
                        "given in the UK and lasts for 13 weeks. Occasionally, Noristerat may be given, which lasts for " +
                        "8 weeks. Sayana Press also lasts for 13 weeks, but it's a newer type of injection so is not " +
                        "available at all clinics or GP surgeries.")
            }
            "intrauterine devices" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("There are two types of IUDs available, the copper IUD and the progestin IUD. With the " +
                        "copper IUD, spotting between periods and heavier, longer, and more painful periods are common " +
                        "in the first three to six months. Most women find this improves over time, and normal or " +
                        "near-normal periods resume after a few months. With a progestin IUD, spotting between periods " +
                        "and irregular periods are common in the first three to six months. Usually this improves over " +
                        "time and many women ultimately have light or absent periods with the progestin IUD. " +
                        "The progestin IUD can be effective for many years (a new one is reinserted after 5 years). " +
                        "'A smaller mini-IUD is also available, and may be preferable for women who have not had a child.")
            }
            "emergency contraceptives" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("Emergency contraception is also known as the \"morning after pill\". Emergency " +
                        "contraception is not to be used as a regular method of birth control but, if needed, it can " +
                        "help prevent unplanned pregnancies.The ‘morning after’ pill may affect the length of your" +
                        " menstrual cycle, causing your period to come earlier or later than you were expecting it to." +
                        " Some women also experience spotting between periods after taking emergency contraception. " +
                        "Your next menstrual cycle may also be slightly longer than normal, but if your next period " +
                        "is more than a couple of days late, it is a good idea to use a pregnancy test.")
            }
            "female condom" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("Female condoms are made from soft, thin synthetic latex or latex. They're worn inside " +
                        "the vagina to prevent semen getting to the womb. If used correctly, female condoms are 95% " +
                        "effective. They protect against pregnancy and sexually transmitted infections (STIs). " +
                        "Female condoms may not be suitable for women who are not comfortable touching their genital area.")
            }
            "contraceptive diaphragm or cap" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("A diaphragm or cap is a barrier method of contraception. It fits inside your vagina " +
                        "and prevents sperm passing through the cervix (the entrance of your womb). You need to use it" +
                        " with a gel that kills sperm (spermicide). You only have to use a diaphragm or cap when you " +
                        "have sex, but you must leave it in for at least 6 hours after the last time you had sex. You " +
                        "can leave it in for longer than this, but don't take it out before.")
            }
        }

        furhat.ask("You have heard about ${contraceptiveMethodsList.text}. Do you want to continue in the same category?")

    }

    onReentry {
        furhat.ask("Did you want anything else?")
    }

    onResponse<No> {
        furhat.say("Okay, no problem.")
        goto(ChooseTopic)
    }

}

var ChooseContraceptiveMethod = state (parent = ContraceptionMethods){
    onEntry {
        furhat.say("That's a very interesting topic. Contraception is not only important to prevent unwanted pregnancy but also may protect you from STIs.")
        furhat.ask("I am familiar with the following methods: female condom, contraceptive diaphragm or cap, birth control pills, injected contraceptives, vaginal ring or intrauterine devices. Choose one to discuss more.")
    }

    onResponse<No> {
        furhat.say("Okay, that's a shame. Have a splendid day!")
        goto(Idle)
    }
}

//Interaction inside Menstruation Products (Options Node)
var MenstruationProducts = state (Interaction) {
    val menstrualProductsOptionsMsg = "I am familiar with the following products: Tampons, Menstrual cup and Pads. Choose one to discuss more."
    onResponse<MenstruationProductsIntent> {
        val menstrualProducts = it.intent.menstruationProducts
        if (menstrualProducts != null) {
            goto(MenstruationProductSelected(menstrualProducts))
        }
        else {
            propagate()
        }
    }

    onResponse<RequestContraceptiveMethods> {
        furhat.gesture(Gestures.Thoughtful)
        furhat.ask(menstrualProductsOptionsMsg)
    }

    onResponse<Yes> {
        furhat.gesture(Gestures.Thoughtful)
        furhat.ask(menstrualProductsOptionsMsg)
    }

    onResponse<GoodbyeKeywords> {
        furhat.say(
                "Ok bye!"
        )
        goto(Idle)
    }
}

fun MenstruationProductSelected(menstruationProductList: MenstruationProductsList) : State = state (MenstruationProducts){
    onEntry {
        furhat.say(
                "${menstruationProductList.text}!"
        )

        when (menstruationProductList.text) {
            "tampons" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("If you want to, you can use tampons from the beginning of your first period. " +
                        "Just check the instructions or ask for tips from a family member, healthcare provider, " +
                        "or friend. Choose the right absorbency for your flow. The best way to know if your tampon " +
                        "needs changing is to give a light pull on the tampon string. If it starts to pull out easily, " +
                        "then it's time to change it; if not, it usually means you can leave it a bit longer. Do not " +
                        "leave a tampon in for more than 8 hours as this increases the risk for developing Toxic Shock Syndrome.")
            }
            "menstrual cup" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("Menstrual cups are a reusable collection method for menstrual fluid during your period." +
                        " Although they may look daunting at first, with a little practice, using a menstrual cup is an " +
                        "easy, safe, environmentally-friendly, and economical way to collect menstrual blood.")
            }
            "pads" -> {
                furhat.gesture(Gestures.Nod)
                furhat.say("A disposable pad, you only need to unfold them and stick them on your underwear!")
            }
        }

        furhat.ask("You have heard about ${menstruationProductList.text}. Do you want to continue in the same category?")
    }

    onReentry {
        furhat.ask("Do you want anything else?")
    }

    onResponse<No> {
        furhat.say("Okay, no problem.")
        goto(ChooseTopic)
    }
}

//Interaction inside Menstruation Products
var ChooseMenstruationProducts = state (parent = MenstruationProducts){
    onEntry {
        furhat.say("That's a very interesting topic. Menstruation products are an inevitable part of your monthly routine. Let's see.")
        furhat.ask("I am familiar with the following products: Tampons, Menstrual cup and pads. Choose one to discuss more.")
    }

    onResponse<No> {
        furhat.say("Okay, that's a shame. Have a splendid day!")
        goto(Idle)
    }
}
